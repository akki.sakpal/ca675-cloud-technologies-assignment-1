/*Hive Query for Top 10 Spam Emails*/
SELECT contact_info
FROM emails
WHERE predicted_spam = 1
ORDER BY spam_probability DESC
LIMIT 10;
/*Hive Query for Top 10 Ham Emails*/
SELECT contact_info
FROM emails
WHERE predicted_spam = 0
ORDER BY spam_probability
LIMIT 10;